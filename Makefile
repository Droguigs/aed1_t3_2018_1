CC=gcc

CFLAGS=-Wall -Wextra -Werror -O0 -g -std=c11 -I..
LDFLAGS=-lm

.PHONY: all clean

all: test_aluno test_professor

arvore.o: arvore.c
le.o: le.c
aluno.o: aluno.c
test.o: test.c

aluno: arvore.o le.o aluno.o $(LDFLAGS)

test: arvore.o le.o test.o $(LDFLAGS)


test_aluno: aluno
	./aluno

test_professor: test
	./test

clean:
	rm -f *.o aluno test all

