//
// Created by Schiavi on 21/05/2018.
//

#include <stdlib.h>
#include <stdio.h>
#include "arvore.h"

struct nodo * inicializa_arvore(int n, int * elems){
  if(n <= 0 || elems == NULL){
    return NULL;
  }
  int i;
  struct nodo * head;
  head = insere_nodo(0, elems[0]);
  for(i=1; i<n; i++){
      head = insere_nodo(head, elems[i]);
  }
  return head;
}

struct nodo * insere_nodo(struct nodo * head, int elem){
  if (head == 0){
      head = malloc(sizeof(struct nodo));
      head->valor = elem;
      head->dir = NULL;
      head->esq = NULL;
      head->bal = 0;
      return head;
  }else{
      if(elem > head->valor){
          head->dir = insere_nodo(head->dir, elem);
      }else if(elem < head->valor){
          head-> esq = insere_nodo(head->esq, elem);
      }
  }

  //Refresh balanceamento
  head->bal = balanceada(head);

  if(head->bal > 1){
    if(head->dir->bal < 0){
      head = rotacaoDuplaEsquerda(head);
    }else{
      head = rotacaoSimplesEsquerda(head);
    }
  }else if(head->bal < -1){
    if(head->esq->bal > 0){
      head = rotacaoDuplaDireita(head);
    }else{
      head = rotacaoSimplesDireita(head);
    }
  }

  return head;
}

struct nodo * remove_nodo(struct nodo * head, int elem){
  if (head == 0){
    return head;
  }else{
    if(elem > head->valor){
      head->dir = remove_nodo(head->dir, elem);
    }else if(elem < head->valor){
      head->esq = remove_nodo(head->esq, elem);
    }else {
      struct nodo* aux;
      if(head->esq==0){
        aux = head->dir;
        free(head);
        if (aux!= NULL) {
          aux->bal = balanceada(aux);
        }return aux;
      } else if(head->dir == 0){
        aux = head->esq;
        free (head);
        if (aux!= NULL) {
          aux->bal = balanceada(aux);
        }return aux;
      } else{
        aux = inorderSuccessor(head->dir);
        head->valor = aux->valor;
        head->dir = remove_nodo(head->dir, aux->valor);
      }
    }
  }

  //Refresh balanceamento
  head->bal = balanceada(head);

  if(head->bal > 1){
    if(head->dir->bal < 0){
      head = rotacaoDuplaEsquerda(head);
    }else{
      head = rotacaoSimplesEsquerda(head);
    }
  }else if(head->bal < -1){
    if(head->esq->bal > 0){
      head = rotacaoDuplaDireita(head);
    }else{
      head = rotacaoSimplesDireita(head);
    }
  }

  return head;
}

int altura(struct nodo * head){
  if(head == NULL) {
    return 0;
  }
  return 1 + max(altura(head->esq), altura(head->dir));
}

struct nodo * busca(struct nodo * node, int elem){
  if (node == NULL){
    return NULL;
  }else if(node->valor == elem){
      return node;
  }else if(elem < node->valor){
      return busca(node->esq, elem);
  }return busca(node->dir, elem);
}

int balanceada(struct nodo * head){
  if (head == NULL){
    return 0;
  }
  return altura(head->dir) - altura(head->esq);
}

int numero_elementos(struct nodo * head){
  if(head == 0){
    return 0;
  }
  return 1 + numero_elementos(head->esq) + numero_elementos(head->dir);
}

int abrangencia(struct nodo * head, int * resultado){
    struct llist* tmp;
    struct nodo* active;
    int i = 0;

    tmp = create_l();
    active = head;
    if (active != NULL) {
        enQueue(&active, tmp);
    }while (tmp->head!=NULL) {
        active = deQueue(tmp);
        percorre(tmp);
        if (active->esq != NULL) {
            enQueue(&active->esq, tmp);
        }
        if (active->dir != NULL) {
            enQueue(&active->dir, tmp);
        }
        resultado[i] = active->valor;
        i++;
    }
    return i;
}

int prefix(struct nodo * head, int * result){
  // H - L - R
  int j;
  if(head == NULL) {
      return 0;
  }
  result[0]= head->valor;
  j = 1 + prefix(head->esq, &result[1]);
  j = j + prefix(head->dir, &result[j]);
  return j;
}

int postfix(struct nodo * head, int * result){
  // L - H - R
  int j;
  if(head == NULL) {
      return 0;
  }
  j = 1 + postfix(head->esq, result);
  result[j-1] = head->valor;
  return j + postfix(head->dir, &result[j]);
}

int infix(struct nodo * head, int * result){
  // L - R - H
  int j;
  if(head == NULL) {
      return 0;
  }
  j = 1 + infix(head->esq, result);
  j = j + infix(head->dir, &result[j-1]);
  result[j-1] = head->valor;
  return j;
}

struct nodo* inorderSuccessor(struct nodo* node){
  if(node->esq == 0){
    return node;
  }
  return inorderSuccessor(node->esq);
}

void imprime(int * valores, int tamanho){
  for(int i = 0; i<tamanho; i++){
    if(i>0 && i%10 == 0){
      printf("\n");
    }
    printf("%d ", valores[i]);
  }
}

struct nodo* remove_todos(struct nodo * head){
  if(head == 0){
    return 0;
  }
  head = remove_nodo(head, head->valor);
  return remove_todos(head);
}

int max(int a, int b){
  return (a >= b)? a: b;
}

struct llist * enQueue(struct nodo **head, struct llist *list){
    if(length_l(list) > 0) {
      insert_l(list, get_l(list, length_l(list)), create_node(head));
    }else{
      insert_l(list, 0, create_node(head));
    }
    return list;
}

struct nodo* deQueue(struct llist *list){
    elem *head;
    head = get_l(list, 1);

    return *head->val;
}

struct llist * percorre(struct llist *list){
    delete_l(list, list->head);

    return list;
}

struct nodo* rotacaoSimplesDireita(struct nodo *k2){
    struct nodo *k1;

    // k1  - Pivot; k2 - Raiz
    k1 = k2->esq;
    k2->esq = k1->dir;
    k1->dir = k2;

    k1->bal = balanceada(k1);
    k2->bal = balanceada(k2);

    return k1;
}

struct nodo* rotacaoSimplesEsquerda(struct nodo *k2){
    struct nodo *k1;

    // k1  - Pivot; k2 - Raiz
    k1 = k2->dir;
    k2->dir = k1->esq;
    k1->esq = k2;

    k1->bal = balanceada(k1);
    k2->bal = balanceada(k2);

    return k1;
}

struct nodo* rotacaoDuplaDireita(struct nodo *head){
    head->esq = rotacaoSimplesEsquerda(head->esq);
    return rotacaoSimplesDireita(head);
}

struct nodo* rotacaoDuplaEsquerda(struct nodo *head){
    head->dir = rotacaoSimplesDireita(head->dir);
    return rotacaoSimplesEsquerda(head);
}